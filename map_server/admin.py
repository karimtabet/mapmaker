from django.contrib import admin
from map_server.models import *


class UserSubmissionAdmin(admin.ModelAdmin):
    list_display = ('email', 'photo','description', 'approval_status', 'uid')

admin.site.register(UserSubmission, UserSubmissionAdmin)
admin.site.register(Map)
admin.site.register(Layer)
admin.site.register(LayerDataset)
admin.site.register(MarkerIcon)
admin.site.register(ExclusionBox)
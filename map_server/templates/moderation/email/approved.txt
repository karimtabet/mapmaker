{% if name %}Dear {{ name }}{% else %}Hi there{% endif %}

Thanks for helping save bees by donating to The Bee Cause. If you've requested our Bee Saver Kit, you should have received it already. If not, it is winging its way to you right now.

To say thank you for your support, we've planted a flower for you on our wildflower map. Find your flower and see where other Bee Savers are active in your are: http://click.foe.co.uk/ct.html?ufl=3&rtr=on&s=x8pbzp,1gar5,3zk6,6yuw,ij0p,4ohv,cjk0&MLM_MID=2440048&MLM_UNIQUEID=355038ee00 

With the help of Bee Savers like you, we're making 2013 The Year of the Bee and covering the UK in bee-friendly flowers. By the end of the summer we hope to see wildflowers planted in every town, city and village around the UK. If you can, please help us fill in the gaps on our map by telling your friends.

Have a bee-rilliant day and thank you again for your support.

Paul De Zylva 



To view a web version of this message, use the link below:
http://click.foe.co.uk/ct.html?ufl=3&rtr=on&s=x8pbzp,1gar5,3zk6,dm14,5u1r,4ohv,cjk0&MLM_MID=2440048&MLM_MLID=186054&MLM_SITEID=2010002677&MLM_UNIQUEID=355038ee00

This email was sent by contactus@newsletter.foe.co.uk. To unsubscribe, send an email to contactus@newsletter.foe.co.uk with UNSUBSCRIBE in the subject.
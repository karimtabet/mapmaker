# MIDDLEWARE_CLASSES and INSTALLED_APPS keys are merged

DEBUG = False
TEMPLATE_DEBUG = DEBUG

from os.path import abspath, dirname, join
SITE_ROOT = abspath(dirname(dirname(dirname(__file__))))



# Logging ##############################################################################################################
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'verbose': {'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'},
        'simple': {'format': '%(levelname)s %(message)s'},
    },
    'handlers': {
        'default':
            {'level': 'WARNING',
             'class': 'logging.handlers.RotatingFileHandler',
             'filename': join(SITE_ROOT, 'logs/site.log'),
             'maxBytes': 1024 * 1024 * 1, # 1 MB
             'backupCount': 0,
             'formatter': 'verbose',
            },
        'request_handler':
            {'level': 'WARNING',
             'class': 'logging.handlers.RotatingFileHandler',
             'filename': join(SITE_ROOT, 'logs/request.log'),
             'maxBytes': 1024 * 1024 * 1,
             'backupCount': 0,
             'formatter': 'verbose',
            },
    },
    'loggers': {
        '':
            {'handlers': ['default'],
             'level': 'WARNING',
             'propagate': True
            },
        'django.request': # Stop SQL debug from logging to main logger
            {'handlers': ['request_handler'],
             'level': 'WARNING',
             'propagate': False
            },
        'django.db.backends': # Stop SQL debug from logging to main logger
            {'handlers': ['request_handler'],
             'level': 'WARNING',
             'propagate': False
            },
    }
}
